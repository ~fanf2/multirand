#define unlikely(e) __builtin_expect((e), 0)
#define likely(e)   __builtin_expect((e), 1)

#define CATCAT(a,b) a##b
#define CAT(a,b) CATCAT(a,b)
#define STRSTR(s) #s
#define STR(s) STRSTR(s)

typedef struct bench {
	uint64_t calls;
	double runtime, value;
} s_bench;

typedef struct pcg32 {
	uint64_t state, inc, calls;
} pcg32_t;

extern pcg32_t pcg32_init(void);
extern uint32_t pcg32(pcg32_t *rng);

extern uint32_t nearly(pcg32_t *rng, uint32_t limit);
extern uint32_t really(pcg32_t *rng, uint32_t limit);
extern uint32_t oneper(pcg32_t *rng, uint32_t limit);
extern uint32_t arc4ru(pcg32_t *rng, uint32_t limit);

/*
 * Melissa O'Neill's pcg32 xsh rr random number generator.
 * https://www.pcg-random.org/
 */
static inline uint32_t
pcg32_fast(pcg32_t *rng) {
	uint64_t state = rng->state;
	rng->calls++;
	// Linear Congruential Generator: This "MMIX" multiplier is from
	// Donald Knuth's Art of Computer Programming, volume 2, 3rd edition,
	// section 3.3.4, page 108; Knuth credits Charles Edmund Haynes, Jr.
	rng->state = state * 6364136223846793005ULL + rng->inc;
	// permuted output
	uint32_t xsh = (uint32_t)((state >> 45) ^ (state >> 27));
	uint32_t rr = (uint32_t)(state >> 59);
	return((xsh >> (+rr & 31)) | (xsh << (-rr & 31)));
}

extern uint32_t
slower(pcg32_t *rng, uint32_t limit, uint64_t num);

/*
 * Treat pcg32() as the low half of a 32.32 fixed-point number,
 * so it is a random number in [0,1), and multiply to make a
 * 32.32 fixed-point random number less than the limit. See
 * Donald Knuth's Art of Computer Programming, volume 2, 3rd
 * edition, section 3.6, point (vi) on page 185. Rather than
 * simply truncating as Knuth suggests, use Daniel Lemire's
 * nearly-divisionless algorithm to debias the result.
 */
static inline uint32_t
quickr(pcg32_t *rng, uint32_t limit) {
        uint64_t num = (uint64_t)pcg32_fast(rng) * (uint64_t)limit;
        if(unlikely((uint32_t)(num) < limit))
		return(slower(rng, limit, num));
        return((uint32_t)(num >> 32));
}
