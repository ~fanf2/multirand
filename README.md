nearly-divisionless vs really-divisionless random numbers
=========================================================

See https://dotat.at/@/2022-04-20-really-divisionless.html

> Written by Tony Finch <dot@dotat.at>
> You may do anything with this. It has no warranty.
> <https://creativecommons.org/publicdomain/zero/1.0/>
