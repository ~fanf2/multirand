#define NAME STR(BOUNDED)
#define BENCH CAT(bench_,BOUNDED)

static s_bench
BENCH(pcg32_t *rng, uint32_t limit, uint32_t repeat) {
	uint64_t reset = rng->state;
	rng->calls = 0;
	uint64_t value = 0;
	double scale = (double)limit * (double)repeat;
	double start = doubletime();
	while(repeat --> 0) {
		value += BOUNDED(rng, limit);
	}
	double end = doubletime();
	rng->state = reset;
	return(struct bench){
		.calls = rng->calls,
		.value = 1.0 - 2.0 * value / scale,
		.runtime = end - start,
	};
}

#undef NAME
#undef BENCH
#undef BOUNDED
