#include <stdint.h>

#include "multirand.h"

/*
 * Daniel Lemire's nearly-divisionless unbiased bounded random numbers.
 *
 * https://dotat.at/@/2020-10-29-nearly-divisionless-random-numbers.html
 */
uint32_t
nearly(pcg32_t *rng, uint32_t limit) {
        uint64_t num = (uint64_t)pcg32(rng) * (uint64_t)limit;
        if(unlikely((uint32_t)(num) < limit)) {
		uint32_t residue = (uint32_t)(-limit) % limit;
		while(unlikely((uint32_t)(num) < residue)) {
			num = (uint64_t)pcg32(rng) * (uint64_t)limit;
		}
	}
        return((uint32_t)(num >> 32));
}
